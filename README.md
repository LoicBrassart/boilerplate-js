## INSTRUCTIONS

### Utilisation du backend via `curl`

#### Signup

curl -X POST http://localhost:8080/auth/signup -H 'Content-Type: application/json' -d '{"username":"Guile", "password":"FCKYEAH", "email": "guile@usa.com"}'

#### Login

_Renvoie un objet User et un JWT à transmettre systématiquement ensuite_
curl -X POST http://localhost:8080/auth/login -H 'Content-Type: application/json' -d '{"username":"test@test.com", "password":"wildcode"}'

#### Appel resssource protégée

_Penser à fournir un header Bearer avec le JWT, sinon seules les routes publiques seront accessibles_
curl localhost:8080/authrequired -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJ0ZXN0QHRlc3QuY29tIiwiaWF0IjoxNTQ0NzkxMjUyfQ.VbnKlrq2lJIWoJMxLwGERMtfy-sLabcB76avlduRUzY'

## TODO

### Frontend

- Afficher une notification en cas de login foiré
- Initialiser Webpack
- Utiliser Webpack pour améliorer le support des notifs (fonts entre autres)
- Poursuivre tuto Tyler pour avoir des redirections bien foutues

## DONE

### Backend

- Utiliser Passport.js pour l'auth (user/mdp + JWT)
- Sécuriser des routes pour les rendre inaccessibles aux visiteurs non-identifiés
- Fetcher de vrais utilisateurs quand on appelle l'auth

### Frontend

- Afficher une notification en cas de login réussi
- Rendre des routes inaccessibles aux visiteurs non-identifiés
