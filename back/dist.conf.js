const mysql = require('mysql');

const jwtSecret = 'terrarian yoyo';
const dbHandle = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'boilerplate'
});
const saltRounds = 10;
const portServer = 8080;
module.exports = { jwtSecret, dbHandle, saltRounds, portServer };
