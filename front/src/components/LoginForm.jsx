import React from 'react';
import axios from 'axios';
import { NotificationManager } from 'react-notifications';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const data = {
      username: this.state.username,
      password: this.state.password,
    };
    const config = {};
    const backend = axios.create({
      baseURL: 'http://localhost:8080',
      timeout: 1000,
      headers: { 'Content-Type': 'application/json' },
    });

    backend
      .post('/auth/login', data, config)
      .then((res) => {
        NotificationManager.success(
          `You're now fully logged in, you should try and go to /myhome !`,
          `Congratulations ${res.data.user.username} !`,
          5000,
        );

        localStorage.setItem('currentUserName', res.data.user.username);
        localStorage.setItem('currentUserID', res.data.user.id);
        localStorage.setItem('currentUsserToken', res.data.token);
        return;
      })
      .catch((error) => {
        console.log('Login error !');
        console.log(error);
      });
  };

  render() {
    return (
      <form method="POST" onSubmit={this.handleSubmit}>
        <input type="text" name="username" onChange={this.handleChange} />
        <input type="password" name="password" onChange={this.handleChange} />
        <button>Send data !</button>
      </form>
    );
  }
}
export default LoginForm;
