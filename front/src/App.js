import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';

import logo from './logo.svg';
import LoginForm from './components/LoginForm';
import UserHome from './components/UserHome';
import HomePage from './components/HomePage';
import './App.css';
import './notifications.css';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      authChecker.getUser() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: props.location },
          }}
        />
      )
    }
  />
);

const authChecker = {
  getUser() {
    return localStorage.getItem('currentUser') || null;
  },
};

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <main>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/login" component={LoginForm} />
            <PrivateRoute path="/myhome" component={UserHome} />
          </Switch>
        </main>
        <NotificationContainer />
      </div>
    );
  }
}

export default App;
